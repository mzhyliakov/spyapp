package example.fizarum.com.spyapptest;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;


public class MainFragment extends Fragment {

    public MainFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        final NavController navController = NavHostFragment.findNavController(this);

        view.findViewById(R.id.btn_call_logs).setOnClickListener((l) -> navController.navigate(R.id.callLogsFragment));
        view.findViewById(R.id.btn_messages).setOnClickListener((l) -> navController.navigate(R.id.messagesFragment));
        view.findViewById(R.id.btn_all_apps).setOnClickListener((l) -> navController.navigate(R.id.allAppsFragment));
        view.findViewById(R.id.btn_get_location).setOnClickListener((l) -> navController.navigate(R.id.geolocationFragment));
        view.findViewById(R.id.btn_cellular_data).setOnClickListener((l) -> navController.navigate(R.id.cellularFragment));
        view.findViewById(R.id.btn_contacts).setOnClickListener((l) -> navController.navigate(R.id.contactsFragment));
        view.findViewById(R.id.btn_wifi_info).setOnClickListener((l) -> navController.navigate(R.id.wiFiDetailsFragment));
        view.findViewById(R.id.btn_sim_track).setOnClickListener((l) -> navController.navigate(R.id.simTrackerFragment));
        view.findViewById(R.id.btn_wifi_aware).setOnClickListener((l) -> navController.navigate(R.id.wiFiAwareFragment));
        view.findViewById(R.id.btn_calendar).setOnClickListener((l)-> navController.navigate(R.id.calendarFragment));
        view.findViewById(R.id.btn_bluetooth).setOnClickListener((l) -> navController.navigate(R.id.bluetoothFragment));
        view.findViewById(R.id.btn_service).setOnClickListener((l) -> navController.navigate(R.id.serviceExampleFragment));
    }
}
