package example.fizarum.com.spyapptest.sim;

import android.os.Bundle;

public interface OnSimStateChangedListener {
    void onSimStateChanged(Bundle args);
}
