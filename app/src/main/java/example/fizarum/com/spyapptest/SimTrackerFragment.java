package example.fizarum.com.spyapptest;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;

import example.fizarum.com.spyapptest.sim.OnSimStateChangedListener;
import example.fizarum.com.spyapptest.sim.SimChangeStateReceiver;
import example.fizarum.com.spyapptest.utils.Constants;


public class SimTrackerFragment extends BaseFragment {

    private BroadcastReceiver simChangeStateReceiver;
    private OnSimStateChangedListener listener;
    private TelephonyManager telephonyManager;

    private TextView textView;

    public SimTrackerFragment() {
        listener = args -> {
            if(textView != null && isAdded()) {
                if(telephonyManager != null) {
                    args.putString("phone", getPhoneNumber(telephonyManager, getActivity()));
                }
                textView.setText(args.toString());
            }
        };
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sim_tracker, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        textView = view.findViewById(R.id.text_view);
    }

    @NonNull
    @Override
    public List<String> getPermissions() {
        return Arrays.asList(Manifest.permission.READ_PHONE_STATE);
    }

    @Override
    public int getRequestId() {
        return Constants.MY_PERMISSIONS_REQUEST_READ_PHONE_STATE;
    }

    @Override
    public void onPermissionGranted() {
        if(getActivity() == null) {
            return;
        }

        simChangeStateReceiver = new SimChangeStateReceiver(listener);
        IntentFilter filter = new IntentFilter(Constants.ACTION_SIM_STATE_CHANGED);

        getActivity().registerReceiver(simChangeStateReceiver, filter);

        telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
    }

    @Override
    public void onPermissionFailed() {
        textView.setText(R.string.provide_permission);
    }

    @Override
    public void onPause() {
        super.onPause();

        if(simChangeStateReceiver != null && getActivity() != null) {
            getActivity().unregisterReceiver(simChangeStateReceiver);
        }
    }

    @SuppressLint("HardwareIds")
    private String getPhoneNumber(TelephonyManager telephonyManager, Context context) {
        if(ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            return telephonyManager.getLine1Number();
        } else {
            return "";
        }
    }
}
