package example.fizarum.com.spyapptest;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.wifi.aware.AttachCallback;
import android.net.wifi.aware.DiscoverySessionCallback;
import android.net.wifi.aware.PeerHandle;
import android.net.wifi.aware.SubscribeConfig;
import android.net.wifi.aware.SubscribeDiscoverySession;
import android.net.wifi.aware.WifiAwareManager;
import android.net.wifi.aware.WifiAwareSession;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.ACCESS_WIFI_STATE;
import static android.Manifest.permission.CHANGE_WIFI_STATE;
import static example.fizarum.com.spyapptest.utils.Constants.MY_PERMISSION_REQUEST_WIFI_AWARE;


public class WiFiAwareFragment extends BaseFragment {

    private WifiAwareManager wifiAwareManager;
    private WifiAwareSession wifiAwareSession;
    private AttachCallback attachCallback;
    private DiscoverySessionCallback discoverySessionCallback;

    private TextView textView;

    private BroadcastReceiver wifiAwareReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O &&
                    wifiAwareManager != null && wifiAwareManager.isAvailable()) {
                Log.d("TAG", "wifi aware enabled and ready for work");
                wifiAwareManager.attach(attachCallback, null);

            } else {
                Log.d("TAG", "wifi aware doesn't available on this device");
                textView.setText("wifi aware isn't available on this device");
            }
        }
    };


    public WiFiAwareFragment() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            discoverySessionCallback = new DiscoverySessionCallback(){
                @Override
                public void onSubscribeStarted(@NonNull SubscribeDiscoverySession session) {
                    super.onSubscribeStarted(session);
                }

                @Override
                public void onServiceDiscovered(PeerHandle peerHandle, byte[] serviceSpecificInfo, List<byte[]> matchFilter) {
                    super.onServiceDiscovered(peerHandle, serviceSpecificInfo, matchFilter);
                    Log.d("TAG", "on new service/device found: " + peerHandle + " with spec info: " + new String(serviceSpecificInfo));
                    if(textView != null && getActivity() != null) {
                        getActivity().runOnUiThread(() -> {
                            textView.setText(textView.getText().toString() +
                                    "\n\non new service/device found: " + peerHandle + " with spec info: " + new String(serviceSpecificInfo));

                        });
                    }
                }
            };


            attachCallback = new AttachCallback() {
                @Override
                public void onAttached(WifiAwareSession session) {
                    super.onAttached(session);
                    wifiAwareSession = session;
                    SubscribeConfig config = new SubscribeConfig.Builder()
                            .setServiceName("Aware_File_Share_Service_Name")
                            .build();

                    wifiAwareSession.subscribe(config, discoverySessionCallback, null);
                }

                @Override
                public void onAttachFailed() {
                    super.onAttachFailed();
                    Log.e("TAG", "on attach failed");
                }
            };
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_wi_fi_aware, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        textView = view.findViewById(R.id.text_view);
    }

    @NonNull
    @Override
    public List<String> getPermissions() {
        return Arrays.asList(ACCESS_WIFI_STATE, CHANGE_WIFI_STATE, ACCESS_FINE_LOCATION);
    }

    @Override
    public int getRequestId() {
        return MY_PERMISSION_REQUEST_WIFI_AWARE;
    }

    @Override
    public void onPermissionGranted() {
        if(getActivity() == null) {
            return;
        }

        final Context context = getActivity();

        if(isWiFiSupports(context)) {
            wifiAwareManager = obtainWiFiAwareManager(context);
            if(wifiAwareReceiver == null) {
                IntentFilter filter = new IntentFilter(WifiAwareManager.ACTION_WIFI_AWARE_STATE_CHANGED);
                context.registerReceiver(wifiAwareReceiver, filter);
            } else {
                textView.setText("can't obtain wifi aware manager");
                Log.e("TAG", "can't obtain wifi aware manager");
            }
        } else {
            Log.d("TAG", "wifi aware doesn't supported on this device");
            textView.setText("wifi aware doesn't supported on this device");
        }
    }

    @Override
    public void onPermissionFailed() {

    }

    @Override
    public void onPause() {
        super.onPause();

        if (wifiAwareSession != null) {
            wifiAwareSession.close();
        }

        if(getActivity() != null) {
            try {
                getActivity().unregisterReceiver(wifiAwareReceiver);
            } catch (IllegalArgumentException e) {
                Log.w("TAG", "wifi receiver not subscribed, ignoring unregistering");
            }
        }
    }

    private boolean isWiFiSupports(@NonNull Context context) {
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            return false;
        }
        return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_WIFI_AWARE);
    }


    private WifiAwareManager obtainWiFiAwareManager(@NonNull Context context) {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            return (WifiAwareManager) context.getSystemService(Context.WIFI_AWARE_SERVICE);
        }

        return null;
    }
}
