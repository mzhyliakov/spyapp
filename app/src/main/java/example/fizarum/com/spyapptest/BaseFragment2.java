package example.fizarum.com.spyapptest;

import android.content.Context;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;

import datasources.fizarum.com.DataSource;
import datasources.fizarum.com.OnDataFetchedListener;

public abstract class BaseFragment2 extends Fragment implements OnDataFetchedListener {

    public abstract DataSource getModule();

    private int requestCount;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        getModule().setContext(context);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() == null) {
            return;
        }

        boolean areAllPermissionsGranted = true;
        if (getModule().getPermissions() != null) {
            for (String permission : getModule().getPermissions()) {
                if (ContextCompat.checkSelfPermission(getActivity(), permission) != PackageManager.PERMISSION_GRANTED) {
                    areAllPermissionsGranted = false;
                }
            }
        }

        if (!areAllPermissionsGranted) {
            if (requestCount >= DataSource.MAX_REQUESTS_COUNT) {
                getModule().onPermissionFailed();
                getModule().onStop();
                return;
            }
            requestCount++;
            String[] permissionsArray = (String[]) getModule().getPermissions().toArray();
            if(permissionsArray != null) {
                ActivityCompat.requestPermissions(getActivity(), permissionsArray, getModule().getRequestId());
            }
        } else {
            getModule().onPermissionGranted();
            getModule().onStart();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == getModule().getRequestId()) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && getActivity() != null) {
                getModule().onPermissionGranted();
                getModule().onStart();
            } else {
                getModule().onPermissionFailed();
                getModule().onStop();
            }
        }
    }
}
