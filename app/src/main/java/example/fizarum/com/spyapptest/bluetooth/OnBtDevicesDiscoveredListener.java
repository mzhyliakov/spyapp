package example.fizarum.com.spyapptest.bluetooth;

import android.bluetooth.BluetoothDevice;

public interface OnBtDevicesDiscoveredListener {
    void OnBtDevicesDiscovered(BluetoothDevice discoveredDevice);
}
