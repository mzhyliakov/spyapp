package example.fizarum.com.spyapptest;

import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;

import java.util.List;

public abstract class BaseFragment extends Fragment {

    private static final int MAX_REQUESTS_COUNT = 5;

    public abstract @NonNull
    List<String> getPermissions();

    public abstract int getRequestId();

    public abstract void onPermissionGranted();

    public abstract void onPermissionFailed();

    int requestCount;

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() == null) {
            return;
        }

        boolean areAllPermissionsGranted = true;
        for(String permission : getPermissions()) {
            if(ContextCompat.checkSelfPermission(getActivity(), permission) != PackageManager.PERMISSION_GRANTED) {
                areAllPermissionsGranted = false;
            }
        }

        if (!areAllPermissionsGranted) {
            if (requestCount >= MAX_REQUESTS_COUNT) {
                onPermissionFailed();
                return;
            }
            requestCount++;
            String[] permissionsArray = (String[]) getPermissions().toArray();
            if(permissionsArray != null) {
                ActivityCompat.requestPermissions(getActivity(), permissionsArray, getRequestId());
            }
        } else {
            onPermissionGranted();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == getRequestId()) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && getActivity() != null) {
                onPermissionGranted();
            } else {
                onPermissionFailed();
            }
        }
    }
}

