package example.fizarum.com.spyapptest.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class WiFiNetworkStatChangeReceiver extends BroadcastReceiver {

    WiFiChangeListener listener;

    public WiFiNetworkStatChangeReceiver(WiFiChangeListener listener) {
        this.listener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if(listener != null) {
            listener.onNetworkChanged();
        }
    }
}
