package example.fizarum.com.spyapptest.bluetooth;

public interface OnBtEnabledListener {
    void onBtEnabled();
}
