package example.fizarum.com.spyapptest.network;

public interface WiFiChangeListener {

    void onNetworkChanged();
}
