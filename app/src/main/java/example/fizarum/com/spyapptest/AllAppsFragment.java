package example.fizarum.com.spyapptest;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import datasources.fizarum.com.DataSource;
import datasources.fizarum.com.sources.InstalledAppsSource;


public class AllAppsFragment extends BaseFragment2 {

    private TextView textView;

    private DataSource dataSource;

    public AllAppsFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_all_apps, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        textView = view.findViewById(R.id.apps_tv);
    }

    @Override
    public DataSource getModule() {
        if (dataSource == null) {
            dataSource = new InstalledAppsSource();
            dataSource.setOnDataFetchListener(this);
        }
        return dataSource;
    }

    @Override
    public void onData(List<Bundle> data) {
        StringBuilder sb = new StringBuilder();
        for (Bundle record : data) {
            sb.append(record);
            sb.append("\n\n");
        }
        textView.setText(sb.toString());
    }

    @Override
    public void onFailed(Throwable error) {
        textView.setText(error.getMessage());
    }
}