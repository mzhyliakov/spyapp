package example.fizarum.com.spyapptest.utils;

public class Constants {

    private Constants() {
    }

    public final static int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 100;
    public final static int MY_PERMISSIONS_REQUEST_READ_SMS = 101;
    public final static int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 102;
    public final static int MY_PERMISSIONS_REQUEST_LOCATION = 103;
    public final static int MY_PERMISSIONS_REQUEST_NETWORK_STATE = 104;
    public final static int MY_PERMISSIONS_REQUEST_ACCESS_WIFI_STATE = 105;
    public final static int MY_PERMISSION_REQUEST_WIFI_AWARE = 106;
    public final static int MY_PERMISSION_REQUEST_READ_CALENDAR = 107;
    public final static int MY_PERMISSION_REQUEST_ACCESS_BLUETOOTH = 108;
    public final static int MY_PERMISSIONS_REQUEST_READ_CALL_LOGS = 109;

    public final static String ACTION_SIM_STATE_CHANGED = "android.intent.action.SIM_STATE_CHANGED";
}
