package example.fizarum.com.spyapptest;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import datasources.fizarum.com.DataSource;
import datasources.fizarum.com.sources.CalendarSource;


public class CalendarFragment extends BaseFragment2 {

    private TextView eventsTextView;

    private DataSource dataSource;

    public CalendarFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_calendar, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        eventsTextView = view.findViewById(R.id.events_text_view);
    }

    @Override
    public DataSource getModule() {
        if(dataSource == null) {
            dataSource = new CalendarSource();
            dataSource.setOnDataFetchListener(this);
        }
        return dataSource;
    }

    @Override
    public void onData(List<Bundle> data) {
        StringBuilder sb = new StringBuilder();
        for(Bundle record : data) {
            sb.append(record);
            sb.append("\n\n");
        }
        eventsTextView.setText(sb.toString());
    }

    @Override
    public void onFailed(Throwable error) {
        eventsTextView.setText(error.getMessage());
    }
}
