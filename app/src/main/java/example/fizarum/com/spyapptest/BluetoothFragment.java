package example.fizarum.com.spyapptest;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import example.fizarum.com.spyapptest.bluetooth.BtDiscoveredReceiver;
import example.fizarum.com.spyapptest.bluetooth.OnBtDevicesDiscoveredListener;
import example.fizarum.com.spyapptest.bluetooth.OnBtEnabledListener;
import example.fizarum.com.spyapptest.bluetooth.Utils;
import example.fizarum.com.spyapptest.utils.Constants;


public class BluetoothFragment extends BaseFragment implements OnBtEnabledListener,
        OnBtDevicesDiscoveredListener {

    private static final int REQUEST_ENABLE_BT = 1;

    private TextView textView;
    private TextView textView2;

    private BtDiscoveredReceiver btDiscoveredReceiver;

    //list of cached devices. its not necessary to store them right now,
    //but added for further usage
    private List<BluetoothDevice> pairedDevices = new ArrayList<>();
    private List<BluetoothDevice> newDevices = new ArrayList<>();

    public BluetoothFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_bluetooth, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        textView = view.findViewById(R.id.text_view);
        textView2 = view.findViewById(R.id.text_view2);
    }

    @Override
    public void onPause() {
        super.onPause();
        if(getActivity() != null && btDiscoveredReceiver != null) {
            try {
                getActivity().unregisterReceiver(btDiscoveredReceiver);
            } catch (IllegalStateException e) {
                Log.w("TAG", "receiver not registered, ignoring");
            }
        }
    }

    @NonNull
    @Override
    public List<String> getPermissions() {
        return Arrays.asList(Manifest.permission.BLUETOOTH,
                Manifest.permission.BLUETOOTH_ADMIN,
                Manifest.permission.ACCESS_FINE_LOCATION);
    }

    @Override
    public int getRequestId() {
        return Constants.MY_PERMISSION_REQUEST_ACCESS_BLUETOOTH;
    }

    @Override
    public void onPermissionGranted() {
        Log.d("TAG", "bt permission granted");
        if(!isBtSupported()) {
            textView.setText("bluetooth doesn't supported on this device");
            return;
        }

        enableBt();
    }

    @Override
    public void onPermissionFailed() {
        textView.setText(R.string.provide_permission);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_ENABLE_BT && resultCode == Activity.RESULT_OK) {
            onBtEnabled();
        }
    }

    @Override
    public void onBtEnabled() {
        Log.d("TAG", "bt enabled");
        textView.setText("bt enabled");

        Set<BluetoothDevice> paired = getPairedDevices();
        pairedDevices.addAll(paired);

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("paired devices:-------------------------\n");
        for(BluetoothDevice device : paired) {
            stringBuilder.append(Utils.toString(device));
            stringBuilder.append("\n\n");
        }

        stringBuilder.append("new devices:-------------------------\n");
        textView.setText(stringBuilder.toString());

        Log.d("TAG", stringBuilder.toString());

        if(getActivity() != null) {
            startDiscoveringDevices(getActivity(), this);
        }
    }

    @Override
    public void OnBtDevicesDiscovered(BluetoothDevice discoveredDevice) {
        if (!newDevices.contains(discoveredDevice)) {
            newDevices.add(discoveredDevice);
        }
        StringBuilder stringBuilder = new StringBuilder();
        for (BluetoothDevice device : newDevices) {
            String deviceStr = Utils.toString(device) + "\n\n";
            stringBuilder.append(deviceStr);
        }
        textView2.setText(stringBuilder.toString());
    }

    private boolean isBtSupported() {
        return BluetoothAdapter.getDefaultAdapter() != null;
    }

    private void enableBt() {
        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
        if(adapter.isEnabled()) {
            onBtEnabled();
            return;
        }

        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
    }

    private Set<BluetoothDevice> getPairedDevices() {
        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
        return adapter.getBondedDevices();
    }

    private void startDiscoveringDevices(@NonNull Context context, OnBtDevicesDiscoveredListener listener) {
        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();

        btDiscoveredReceiver = new BtDiscoveredReceiver(listener);
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        context.registerReceiver(btDiscoveredReceiver, filter);

        adapter.startDiscovery();
    }
}
