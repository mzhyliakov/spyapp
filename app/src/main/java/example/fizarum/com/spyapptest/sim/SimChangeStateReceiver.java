package example.fizarum.com.spyapptest.sim;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import java.util.Set;

public class SimChangeStateReceiver extends BroadcastReceiver {

    private final OnSimStateChangedListener listener;

    public SimChangeStateReceiver(OnSimStateChangedListener listener) {
        this.listener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("TAG", "on sim state changed: " + intent.getAction());

        Bundle extras = intent.getExtras();
        if(extras == null || extras.isEmpty()) {
            return;
        }

        if(listener != null) {
            listener.onSimStateChanged(extras);
        }

        StringBuilder stringBuilder = new StringBuilder();
        Set<String> keys = extras.keySet();
        for(String key : keys) {
            stringBuilder.append('[');
            stringBuilder.append(key);
            stringBuilder.append("] = ");
            stringBuilder.append(extras.getString(key));
            stringBuilder.append('\n');
        }

        Log.d("TAG", stringBuilder.toString());
    }
}
