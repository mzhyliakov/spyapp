package example.fizarum.com.spyapptest;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;

import example.fizarum.com.spyapptest.network.WiFiChangeListener;
import example.fizarum.com.spyapptest.network.WiFiNetworkStatChangeReceiver;

import static android.net.wifi.WifiManager.SCAN_RESULTS_AVAILABLE_ACTION;
import static example.fizarum.com.spyapptest.utils.Constants.MY_PERMISSIONS_REQUEST_ACCESS_WIFI_STATE;


public class WiFiDetailsFragment extends BaseFragment {

    private static final String TAG = WiFiDetailsFragment.class.getSimpleName();

    private TextView textView;
    private BroadcastReceiver networkStateReceiver;
    private WiFiChangeListener wiFiChangeListener;
    private WifiManager wifiManager;

    public WiFiDetailsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        wiFiChangeListener = () -> {
            if(textView != null && isAdded()) {
                Log.d(TAG, "on network changed");
                textView.setText(getWiFiInfo(wifiManager));
            }
        };
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_wi_fi_details, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        textView = view.findViewById(R.id.text_view);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
    }

    @NonNull
    @Override
    public List<String> getPermissions() {
        return Arrays.asList(Manifest.permission.ACCESS_WIFI_STATE, Manifest.permission.ACCESS_FINE_LOCATION);
    }

    @Override
    public int getRequestId() {
        return MY_PERMISSIONS_REQUEST_ACCESS_WIFI_STATE;
    }

    @Override
    public void onPermissionGranted() {
        if(getActivity() == null) {
            return;
        }

        networkStateReceiver = new WiFiNetworkStatChangeReceiver(wiFiChangeListener);
        IntentFilter filter = new IntentFilter(SCAN_RESULTS_AVAILABLE_ACTION);
        getActivity().registerReceiver(networkStateReceiver, filter);

        wifiManager.startScan();
        textView.setText(getWiFiInfo(wifiManager));
    }

    @Override
    public void onPermissionFailed() {
        textView.setText(R.string.provide_permission);
    }

    @Override
    public void onPause() {
        super.onPause();

        if(networkStateReceiver != null && getActivity() != null) {
            getActivity().unregisterReceiver(networkStateReceiver);
        }
    }

    private String getWiFiInfo(WifiManager wifiManager) {
        if(getActivity() == null) {
            return "";
        }

        if(wifiManager == null || !wifiManager.isWifiEnabled()) {
            return "";
        }

        StringBuilder stringBuilder = new StringBuilder();

        WifiInfo info = wifiManager.getConnectionInfo();
        if(info != null) {
            stringBuilder.append("active: \n");
            stringBuilder.append(info.toString());
        } else {
            textView.setText(R.string.cant_obtain_wifi);
            return "";
        }

        List<ScanResult> scanResults = wifiManager.getScanResults();
        stringBuilder.append("\n\nlist: \n");
        for(ScanResult result : scanResults) {
            stringBuilder.append(result.toString());
            stringBuilder.append('\n');
            stringBuilder.append('\n');
        }
        stringBuilder.append('\n');

        return stringBuilder.toString();
    }

}
