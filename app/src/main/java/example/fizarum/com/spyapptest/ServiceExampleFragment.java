package example.fizarum.com.spyapptest;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import background.fizarum.com.MainBackgroundService;


public class ServiceExampleFragment extends Fragment {

    Button btnStartService;
    Button btnStopService;

    public ServiceExampleFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_service_example, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        btnStartService = view.findViewById(R.id.btn_start_service);
        btnStopService = view.findViewById(R.id.btn_stop_service);

        btnStartService.setOnClickListener((l) -> {
            Intent intent = new Intent(view.getContext(), MainBackgroundService.class);
            view.getContext().getApplicationContext().startService(intent);
        });

        btnStopService.setOnClickListener((l) -> {
        });
    }
}
