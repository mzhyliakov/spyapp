package example.fizarum.com.spyapptest.bluetooth;

import android.bluetooth.BluetoothDevice;

import static android.bluetooth.BluetoothClass.Device.*;

public class Utils {

    private Utils() {
    }

    public static String toString(BluetoothDevice device) {
        return String.format("name: %s\taddr: %s\tclass: %s",
                device.getName(),
                device.getAddress(),
                getDeviceClass(device));
    }

    public static String getDeviceClass(BluetoothDevice device) {
        switch (device.getBluetoothClass().getDeviceClass()) {
            case AUDIO_VIDEO_CAMCORDER:
                return "AUDIO_VIDEO_CAMCORDER";
            case AUDIO_VIDEO_CAR_AUDIO:
                return "AUDIO_VIDEO_CAR_AUDIO";
            case AUDIO_VIDEO_HANDSFREE:
                return "AUDIO_VIDEO_HANDSFREE";
            case AUDIO_VIDEO_HEADPHONES:
                return "AUDIO_VIDEO_HEADPHONES";
            case AUDIO_VIDEO_HIFI_AUDIO:
                return "AUDIO_VIDEO_HIFI_AUDIO";
            case AUDIO_VIDEO_LOUDSPEAKER:
                return "AUDIO_VIDEO_LOUDSPEAKER";
            case AUDIO_VIDEO_MICROPHONE:
                return "AUDIO_VIDEO_MICROPHONE";
            case AUDIO_VIDEO_PORTABLE_AUDIO:
                return "AUDIO_VIDEO_PORTABLE_AUDIO";
            case AUDIO_VIDEO_SET_TOP_BOX:
                return "AUDIO_VIDEO_SET_TOP_BOX";
            case AUDIO_VIDEO_UNCATEGORIZED:
                return "AUDIO_VIDEO_UNCATEGORIZED";
            case AUDIO_VIDEO_VCR:
                return "AUDIO_VIDEO_VCR";
            case AUDIO_VIDEO_VIDEO_CAMERA:
                return "AUDIO_VIDEO_VIDEO_CAMERA";
            case AUDIO_VIDEO_VIDEO_CONFERENCING:
                return "AUDIO_VIDEO_VIDEO_CONFERENCING";
            case AUDIO_VIDEO_VIDEO_DISPLAY_AND_LOUDSPEAKER:
                return "AUDIO_VIDEO_VIDEO_DISPLAY_AND_LOUDSPEAKER";
            case AUDIO_VIDEO_VIDEO_GAMING_TOY:
                return "AUDIO_VIDEO_VIDEO_GAMING_TOY";
            case AUDIO_VIDEO_VIDEO_MONITOR:
                return "AUDIO_VIDEO_VIDEO_MONITOR";
            case AUDIO_VIDEO_WEARABLE_HEADSET:
                return "AUDIO_VIDEO_WEARABLE_HEADSET";
            case COMPUTER_DESKTOP:
                return "COMPUTER_DESKTOP";
            case COMPUTER_HANDHELD_PC_PDA:
                return "COMPUTER_HANDHELD_PC_PDA";
            case COMPUTER_LAPTOP:
                return "COMPUTER_LAPTOP";
            case COMPUTER_PALM_SIZE_PC_PDA:
                return "COMPUTER_PALM_SIZE_PC_PDA";
            case COMPUTER_SERVER:
                return "COMPUTER_SERVER";
            case COMPUTER_UNCATEGORIZED:
                return "COMPUTER_UNCATEGORIZED";
            case COMPUTER_WEARABLE:
                return "COMPUTER_WEARABLE";
            case HEALTH_BLOOD_PRESSURE:
                return "HEALTH_BLOOD_PRESSURE";
            case HEALTH_DATA_DISPLAY:
                return "HEALTH_DATA_DISPLAY";
            case HEALTH_GLUCOSE:
                return "HEALTH_GLUCOSE";
            case HEALTH_PULSE_OXIMETER:
                return "HEALTH_PULSE_OXIMETER";
            case HEALTH_PULSE_RATE:
                return "HEALTH_PULSE_RATE";
            case HEALTH_THERMOMETER:
                return "HEALTH_THERMOMETER";
            case HEALTH_UNCATEGORIZED:
                return "HEALTH_UNCATEGORIZED";
            case HEALTH_WEIGHING:
                return "HEALTH_WEIGHING";
            case PHONE_CELLULAR:
                return "PHONE_CELLULAR";
            case PHONE_CORDLESS:
                return "PHONE_CORDLESS";
            case PHONE_ISDN:
                return "PHONE_ISDN";
            case PHONE_MODEM_OR_GATEWAY:
                return "PHONE_MODEM_OR_GATEWAY";
            case PHONE_SMART:
                return "PHONE_SMART";
            case PHONE_UNCATEGORIZED:
                return "PHONE_UNCATEGORIZED";
            case TOY_CONTROLLER:
                return "TOY_CONTROLLER";
            case TOY_DOLL_ACTION_FIGURE:
                return "TOY_DOLL_ACTION_FIGURE";
            case TOY_GAME:
                return "TOY_GAME";
            case TOY_ROBOT:
                return "TOY_ROBOT";
            case TOY_UNCATEGORIZED:
                return "TOY_UNCATEGORIZED";
            case TOY_VEHICLE:
                return "TOY_VEHICLE";
            case WEARABLE_GLASSES:
                return "WEARABLE_GLASSES";
            case WEARABLE_HELMET:
                return "WEARABLE_HELMET";
            case WEARABLE_JACKET:
                return "WEARABLE_JACKET";
            case WEARABLE_PAGER:
                return "WEARABLE_PAGER";
            case WEARABLE_UNCATEGORIZED:
                return "WEARABLE_UNCATEGORIZED";
            case WEARABLE_WRIST_WATCH:
                return "WEARABLE_WRIST_WATCH";
            default:
                return "n/a";
        }
    }
}
