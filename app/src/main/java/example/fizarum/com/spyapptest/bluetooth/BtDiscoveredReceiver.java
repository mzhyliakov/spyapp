package example.fizarum.com.spyapptest.bluetooth;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class BtDiscoveredReceiver extends BroadcastReceiver {

    private OnBtDevicesDiscoveredListener listener;

    public BtDiscoveredReceiver(OnBtDevicesDiscoveredListener listener) {
        this.listener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if(BluetoothDevice.ACTION_FOUND.equals(intent.getAction())) {
            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
            String deviceStr = Utils.toString(device);
            Log.d("TAG", "bt receiver, detected new device: " + deviceStr);
            if(listener != null) {
                listener.OnBtDevicesDiscovered(device);
            }
        }
    }
}
