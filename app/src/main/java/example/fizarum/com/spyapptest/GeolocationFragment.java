package example.fizarum.com.spyapptest;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import datasources.fizarum.com.sources.GeolocationSource;
import datasources.fizarum.com.DataSource;

public class GeolocationFragment extends BaseFragment2 {

    private DataSource module;

    private TextView textView;


    public GeolocationFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_geolocation, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        textView = view.findViewById(R.id.text_view);
    }

    @Override
    public DataSource getModule() {
        if(module == null) {
            module = new GeolocationSource();
            module.setOnDataFetchListener(this);
        }
        return module;
    }

    @Override
    public void onData(List<Bundle> data) {
        StringBuilder sb = new StringBuilder();
        for(Bundle record : data) {
            sb.append(record);
            sb.append("\n\n");
        }
        textView.setText(sb.toString());
    }

    @Override
    public void onFailed(Throwable error) {
        textView.setText(error.getMessage());
    }

    @Override
    public void onPause() {
        super.onPause();
        module.onStop();
    }
}
