package background.fizarum.com;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.util.List;

public class SimplePermissionProvider implements PermissionProvider {

    private Context context;

    public SimplePermissionProvider(Context context) {
        this.context = context.getApplicationContext();
    }

    @Override
    public boolean isPermissionGranted(List<String> permissions) {
        boolean areAllPermissionsGranted = true;

        if (permissions != null && !permissions.isEmpty()) {
            for (String permission : permissions) {
                if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    areAllPermissionsGranted = false;
                }
            }
        }
        return areAllPermissionsGranted;
    }

    @Override
    public void requestPermission(Activity activity, List<String> permissions, int requestId,
                                  RequestPermissionCallback callback) {
        String[] permissionsArray = (String[]) permissions.toArray();
        if (permissionsArray != null) {
            ActivityCompat.requestPermissions(activity, permissionsArray, requestId);
        }
    }


}
