package background.fizarum.com;

import android.app.Activity;

import java.util.List;

public interface PermissionProvider {

    interface RequestPermissionCallback {
        void onAccepted();
        void onCancelled();
    }

    boolean isPermissionGranted(List<String> permissions);
    void requestPermission(Activity activity, List<String> permissions, int requestId, RequestPermissionCallback callback);
}
