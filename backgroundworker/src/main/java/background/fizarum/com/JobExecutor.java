package background.fizarum.com;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import datasources.fizarum.com.DataSource;
import datasources.fizarum.com.sources.CallLogsSource;
import datasources.fizarum.com.sources.InstalledAppsSource;

public class JobExecutor {

    private static final String TAG = JobExecutor.class.getSimpleName();

    private List<Job> waitingList = new LinkedList<>();
    private Job currentJob;
    private ScheduledExecutorService executorService = Executors.newScheduledThreadPool(3);

    public JobExecutor() {
    }

    public void stop() {
        if(currentJob != null) {
            currentJob.cancel();
            currentJob = null;
        }
    }

    public void post(Job job) {
        if(!waitingList.contains(job)) {
            waitingList.add(job);
            update();
        } else {
            Log.w(TAG, "this job already added");
        }
    }

    private void update() {
        final Job job = pickNextJob();
        if(job != null) {
            executorService.schedule(job::start, job.getPeriodMillis(), TimeUnit.MILLISECONDS);
        } else {
            Log.d(TAG, "no job left in waiting list");
        }
    }

    private Job pickNextJob() {
        currentJob = (waitingList.isEmpty()) ? null : waitingList.remove(0);
        return currentJob;
    }

    //region testing

    public Job getTestJob(Context context, PermissionProvider provider) {

        return new Job(context, provider) {
            private DataSource dataSource = new InstalledAppsSource();

            @Override
            protected long getPeriodMillis() {
                return 4000;
            }

            @Override
            protected @NonNull
            DataSource getAttachedDataSource() {
                return dataSource;
            }
        };
    }

    public Job getTestJob2(Context context, PermissionProvider provider) {
        return new Job(context, provider) {

            private DataSource dataSource = new CallLogsSource();

            @Override
            protected long getPeriodMillis() {
                return 5000;
            }

            @NonNull
            @Override
            protected DataSource getAttachedDataSource() {
                return dataSource;
            }
        };
    }

    //endregion
}
