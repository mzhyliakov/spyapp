package background.fizarum.com;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.List;
import java.util.UUID;

import datasources.fizarum.com.DataSource;
import datasources.fizarum.com.OnDataFetchedListener;

/**
 * class represents a job which should be placed to list
 * by planner and executed by main service
 * each job should have its period when it should be executed,
 * data source to obtain {@link DataSource}
 * and permission provider {@link PermissionProvider}
 * to provide required permissions
 */
public abstract class Job implements OnDataFetchedListener {

    private UUID uuid;
    private Context context;
    private PermissionProvider permissionProvider;

    protected abstract long getPeriodMillis();

    protected abstract @NonNull
    DataSource getAttachedDataSource();

    public Job(Context context, PermissionProvider permissionProvider) {
        this.context = context;
        this.permissionProvider = permissionProvider;
        uuid = UUID.randomUUID();
    }

    public void start() {
        String dataSourceName = getAttachedDataSource().getClass().getSimpleName();
        if (permissionProvider.isPermissionGranted(getAttachedDataSource().getPermissions())) {
            Log.d("TAG", "job(" + uuid.toString() + ")[" + dataSourceName + "] started");
            getAttachedDataSource().setContext(context);
            getAttachedDataSource().setOnDataFetchListener(this);
            getAttachedDataSource().onStart();
        } else {
            Log.w("TAG", "job(" + uuid.toString() + ")[" + dataSourceName + "] permission not granted - skip execution");
        }
    }

    public void cancel() {
        String dataSourceName = getAttachedDataSource().getClass().getSimpleName();
        getAttachedDataSource().onStop();
        Log.d("TAG", "job(" + uuid.toString() + ")[" + dataSourceName + "] cancelled");
    }

    @Override
    public void onData(List<Bundle> data) {
        String dataSourceName = getAttachedDataSource().getClass().getSimpleName();
        Log.d("TAG", "job(" + uuid.toString() + ")[" + dataSourceName + "] data: " + data);
    }

    @Override
    public void onFailed(Throwable error) {
        Log.e("TAG", error.getMessage());
    }
}
