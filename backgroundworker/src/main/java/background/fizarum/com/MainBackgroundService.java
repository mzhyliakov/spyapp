package background.fizarum.com;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.concurrent.TimeUnit;

public class MainBackgroundService extends Service {

    private static final long PERIOD_MILLIS = TimeUnit.SECONDS.toMillis(10);

    private Handler handler;
    private JobExecutor jobExecutor;
    private PermissionProvider permissionProvider;

    private Runnable loop = new Runnable() {
        @Override
        public void run() {
            Log.d("TAG", "ping at: " + System.currentTimeMillis());
            handler.postDelayed(this, PERIOD_MILLIS);
        }
    };

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("TAG", "on start command: " + startId);

        Job testJob;
        if(startId % 2 == 0) {
            testJob = jobExecutor.getTestJob(getApplicationContext(), permissionProvider);
        } else {
            testJob = jobExecutor.getTestJob2(getApplicationContext(), permissionProvider);
        }

        jobExecutor.post(testJob);

        handler.removeCallbacksAndMessages(null);
        handler.postDelayed(loop, PERIOD_MILLIS);

        return START_STICKY;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Log.d("TAG", "service removed");
        pingService();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        permissionProvider = new SimplePermissionProvider(getApplicationContext());
        handler = new Handler();
        jobExecutor = new JobExecutor();
    }

    @Override
    public void onDestroy() {
        jobExecutor.stop();
        super.onDestroy();
        Log.d("TAG", "service destroyed");
    }

    private void pingService() {
        Intent restartService = new Intent(getApplicationContext(), this.getClass());

        restartService.setPackage(getPackageName());
        PendingIntent restartServicePI = PendingIntent.getService(
                getApplicationContext(), 1, restartService,
                PendingIntent.FLAG_ONE_SHOT);
        AlarmManager alarmService = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        alarmService.set(AlarmManager.RTC_WAKEUP, SystemClock.elapsedRealtime() + PERIOD_MILLIS, restartServicePI);
    }
}
