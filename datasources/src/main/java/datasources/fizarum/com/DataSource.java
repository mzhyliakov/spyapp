package datasources.fizarum.com;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;

import java.util.List;

public interface DataSource {

    /**
     * count of max retries for obtaining permissionss
     */
    int MAX_REQUESTS_COUNT = 5;

    /**
     * returns list of required permissions
     */
    List<String> getPermissions();

    /**
     * get unique id of requesting permission
     */
    int getRequestId();

    /**
     * called when permission granted
     */
    void onPermissionGranted();

    /**
     * called when permission not granted
     */
    void onPermissionFailed();

    /**
     * called after {@link #onPermissionGranted()}
     */
    void onStart();

    /**
     * called when module is turned off or after {@link #onPermissionFailed()}
     */
    void onStop();

    /**
     * set context of service or application
     * */
    void setContext(@NonNull Context context);

    /**
     * returns fetched specific data
     * */
    List<Bundle> getData();

    /**
     * sets onData fetch listener which will be
     * triggered when data is fetched (of failed)
     * */
    void setOnDataFetchListener(OnDataFetchedListener listener);
}
