package datasources.fizarum.com;

import android.os.Bundle;

import java.util.List;

public interface OnDataFetchedListener {

    void onData(List<Bundle> data);

    void onFailed(Throwable error);
}
