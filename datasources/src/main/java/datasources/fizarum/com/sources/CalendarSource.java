package datasources.fizarum.com.sources;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import datasources.fizarum.com.DataSource;
import datasources.fizarum.com.OnDataFetchedListener;

import static datasources.fizarum.com.Constants.MY_PERMISSION_REQUEST_READ_CALENDAR;

public class CalendarSource implements DataSource {

    private Context context;
    private List<Bundle> fetchedData = new ArrayList<>();
    private OnDataFetchedListener listener;

    @Override
    public List<String> getPermissions() {
        return Arrays.asList(Manifest.permission.READ_CALENDAR);
    }

    @Override
    public int getRequestId() {
        return MY_PERMISSION_REQUEST_READ_CALENDAR;
    }

    @Override
    public void onPermissionGranted() {

    }

    @Override
    public void onPermissionFailed() {

    }

    @Override
    public void onStart() {
        fetchedData.clear();

        ContentResolver contentResolver = context.getContentResolver();

        List<Bundle> calendars = readCalendars(contentResolver);
        for (Bundle calendar : calendars) {
            fetchedData.add(calendar);
            Log.d("TAG", calendar.toString());
            String calendarId = calendar.getString(CalendarContract.Calendars._ID);

            if (!TextUtils.isEmpty(calendarId)) {
                List<Bundle> events = readEvents(contentResolver, calendarId);
                fetchedData.addAll(events);
            }
        }
        if(listener != null) {
            listener.onData(fetchedData);
        }
    }

    @Override
    public void onStop() {

    }

    @Override
    public void setContext(@NonNull Context context) {
        this.context = context;
    }

    @Override
    public List<Bundle> getData() {
        return fetchedData;
    }

    @Override
    public void setOnDataFetchListener(OnDataFetchedListener listener) {
        this.listener = listener;
    }

    private List<Bundle> readCalendars(@NonNull final ContentResolver contentResolver) {
        Uri uri = CalendarContract.Calendars.CONTENT_URI;
        String[] projection = new String[]{
                CalendarContract.Calendars._ID,
                CalendarContract.Calendars.NAME,
                CalendarContract.Calendars.CALENDAR_DISPLAY_NAME,
                CalendarContract.Calendars.VISIBLE,
                CalendarContract.Calendars.OWNER_ACCOUNT
        };

        Cursor cursor = contentResolver.query(uri, projection, null, null, null);

        List<Bundle> result = new ArrayList<>();

        if (cursor == null) {
            return result;
        }

        while (cursor.moveToNext()) {
            Bundle bundle = new Bundle();

            for (int index = 0; index < projection.length; index++) {
                String columnName = cursor.getColumnName(index);
                String columnData = cursor.getString(index);

                bundle.putString(columnName, columnData);
            }
            result.add(bundle);
        }

        cursor.close();
        return result;
    }

    private List<Bundle> readEvents(@NonNull final ContentResolver contentResolver, String calendarId) {
        Uri uri = CalendarContract.Events.CONTENT_URI;

        String[] projection = new String[]{CalendarContract.Events.CALENDAR_ID,
                CalendarContract.Events.ORGANIZER,
                CalendarContract.Events.TITLE,
                CalendarContract.Events.EVENT_LOCATION,
                CalendarContract.Events.DESCRIPTION,
                CalendarContract.Events.DTSTART,
                CalendarContract.Events.DTEND,
                CalendarContract.Events.DURATION,
                CalendarContract.Events.ALL_DAY,
                CalendarContract.Events.RRULE,
                CalendarContract.Events.RDATE
        };

        String selection = CalendarContract.Events.CALENDAR_ID + " = ?";
        String[] selectionArgs = new String[]{calendarId};

        Cursor cursor = contentResolver.query(uri, projection, selection, selectionArgs, null);

        List<Bundle> result = new ArrayList<>();

        if (cursor == null) {
            return result;
        }

        while (cursor.moveToNext()) {
            Bundle bundle = new Bundle();
            for (int index = 0; index < cursor.getColumnCount(); index++) {
                String columnName = cursor.getColumnName(index);
                String columnData = cursor.getString(index);

                bundle.putString(columnName, columnData);
            }
            result.add(bundle);
        }
        cursor.close();

        return result;
    }
}
