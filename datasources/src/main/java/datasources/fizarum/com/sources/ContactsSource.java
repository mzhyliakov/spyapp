package datasources.fizarum.com.sources;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import datasources.fizarum.com.DataSource;
import datasources.fizarum.com.OnDataFetchedListener;

import static datasources.fizarum.com.Constants.MY_PERMISSIONS_REQUEST_READ_CONTACTS;

public class ContactsSource implements DataSource {

    private Context context;
    private OnDataFetchedListener listener;
    private List<Bundle> fetchedData = new ArrayList<>();

    @Override
    public List<String> getPermissions() {
        return Arrays.asList(Manifest.permission.READ_CONTACTS);
    }

    @Override
    public int getRequestId() {
        return MY_PERMISSIONS_REQUEST_READ_CONTACTS;
    }

    @Override
    public void onPermissionGranted() {

    }

    @Override
    public void onPermissionFailed() {

    }

    @Override
    public void onStart() {
        final ContentResolver contentResolver = context.getContentResolver();
        List<Bundle> contacts = getContacts(contentResolver);
        for(Bundle contact : contacts) {
            contact = appendEmail(contact, contentResolver);
            contact = appendPhone(contact, contentResolver);

            fetchedData.add(contact);
        }
        if(listener != null) {
            listener.onData(fetchedData);
        }
    }

    @Override
    public void onStop() {

    }

    @Override
    public void setContext(@NonNull Context context) {
        this.context = context;
    }

    @Override
    public List<Bundle> getData() {
        return fetchedData;
    }

    @Override
    public void setOnDataFetchListener(OnDataFetchedListener listener) {
        this.listener = listener;
    }

    private List<Bundle> getContacts(@NonNull ContentResolver contentResolver) {
        Uri uri = ContactsContract.Contacts.CONTENT_URI;
        String[] projection = new String[] {ContactsContract.Contacts._ID, ContactsContract.Contacts.DISPLAY_NAME_PRIMARY};

        ArrayList<Bundle> result = new ArrayList<>();

        Cursor cursor = contentResolver.query(uri, projection, null, null, null);
        if(cursor == null) {
            return result;
        }

        int idIndex = cursor.getColumnIndex(ContactsContract.Contacts._ID);
        int nameIndex = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME_PRIMARY);

        while(cursor.moveToNext()) {
            String id = cursor.getString(idIndex);
            String name = cursor.getString(nameIndex);

            Bundle data = new Bundle();
            data.putString("id", id);
            data.putString("name", name);
            result.add(data);
        }
        cursor.close();
        return result;
    }

    private Bundle appendEmail(@NonNull Bundle userData, @NonNull ContentResolver contentResolver) {
        Uri uri = ContactsContract.CommonDataKinds.Email.CONTENT_URI;

        String id = userData.getString("id");
        if(TextUtils.isEmpty(id)) {
            return userData;
        }

        String selection = ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?";
        String[] selectionArgs = new String[]{id};

        Cursor cursor = contentResolver.query(uri, null, selection, selectionArgs, null);
        if(cursor == null) {
            return userData;
        }

        int emailIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA);

        //todo add handling multiply emails
        while(cursor.moveToNext()) {
            String email = cursor.getString(emailIndex);
            if(!TextUtils.isEmpty(email)) {
                userData.putString("email", email);
            }
        }
        cursor.close();
        return userData;
    }

    private Bundle appendPhone(Bundle userData, @NonNull ContentResolver contentResolver) {
        Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;

        String id = userData.getString("id");
        if(TextUtils.isEmpty(id)) {
            return userData;
        }


        String selection = ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?";
        String[] selectionArgs = new String[]{id};

        Cursor cursor = contentResolver.query(uri, null, selection, selectionArgs, null);
        if(cursor == null) {
            return userData;
        }

        int phoneIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
        //todo add handling multiply phone numbers
        while(cursor.moveToNext()) {
            String phone = cursor.getString(phoneIndex);
            if (!TextUtils.isEmpty(phone)) {
                userData.putString("phone", phone);
            }
        }
        cursor.close();
        return userData;
    }
}
