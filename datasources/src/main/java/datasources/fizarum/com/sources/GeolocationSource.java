package datasources.fizarum.com.sources;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.CancellationException;

import datasources.fizarum.com.DataSource;
import datasources.fizarum.com.OnDataFetchedListener;

import static datasources.fizarum.com.Constants.MY_PERMISSIONS_REQUEST_LOCATION;

public class GeolocationSource implements DataSource {

    private static final String TAG = GeolocationSource.class.getSimpleName();

    private static final int NORMAL_INTERVAL = 10000;
    private static final int FASTEST_INTERVAL = 5000;
    private static final int PRIORITY = LocationRequest.PRIORITY_HIGH_ACCURACY;

    private OnDataFetchedListener listener;
    private Context context;
    private FusedLocationProviderClient locationProviderClient;
    private LocationCallback locationCallback;
    private final List<Bundle> fetchedData = new ArrayList<>();

    @Override
    public List<String> getPermissions() {
        return Arrays.asList(Manifest.permission.ACCESS_FINE_LOCATION);
    }

    @Override
    public int getRequestId() {
        return MY_PERMISSIONS_REQUEST_LOCATION;
    }

    @Override
    public void onPermissionGranted() {

    }

    @Override
    public void onPermissionFailed() {

    }

    @Override
    public void onStart() {
        startLocationUpdates(context);
    }

    @Override
    public void onStop() {
        stopLocationUpdates();
    }

    @Override
    public void setContext(@NonNull Context context) {
        this.context = context;
        locationProviderClient = LocationServices.getFusedLocationProviderClient(context);
    }

    @Override
    public List<Bundle> getData() {
        return fetchedData;
    }

    @Override
    public void setOnDataFetchListener(OnDataFetchedListener listener) {
        this.listener = listener;
    }

    private void stopLocationUpdates() {
        if (locationProviderClient != null && locationCallback != null) {
            Log.d(TAG, "stop getting location updates");
            locationProviderClient.removeLocationUpdates(locationCallback);
        }
    }

    @SuppressWarnings("unused")
    @SuppressLint("MissingPermission")
    private void getLastKnownLocation(@NonNull final Context context) {
        if (locationProviderClient == null) {
            locationProviderClient = LocationServices.getFusedLocationProviderClient(context);
            locationProviderClient.getLastLocation().
                    addOnSuccessListener(location -> {
                                String locationStr = "last known location: " + location.getLatitude() + " : " + location.getLongitude();
                                Log.d(TAG, locationStr);
                            }
                    );
        }
    }

    @SuppressWarnings("SameParameterValue")
    private LocationRequest createLocationRequest(int normalInterval, int fastestInterval, int priority) {
        LocationRequest request = LocationRequest.create();
        request.setInterval(normalInterval);
        request.setFastestInterval(fastestInterval);
        request.setPriority(priority);
        return request;
    }

    private LocationCallback createLocationCallback() {
        return new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    Log.w(TAG, "location result is null!");
                    return;
                }

                for (Location location : locationResult.getLocations()) {
                    String locationString = String.format(Locale.ROOT,
                            "location lat[%.5f] lot[%.5f] alt[%.5f] at: %d",
                            location.getLatitude(), location.getLongitude(), location.getAltitude(),
                            System.currentTimeMillis());

                    Bundle record = new Bundle();
                    record.putString("lat", String.valueOf(location.getLatitude()));
                    record.putString("lon", String.valueOf(location.getLongitude()));
                    record.putString("alt", String.valueOf(location.getAltitude()));
                    record.putString("date", String.valueOf(System.currentTimeMillis()));
                    synchronized (fetchedData) {
                        if (fetchedData.isEmpty()) {
                            fetchedData.add(record);
                        } else {
                            fetchedData.set(0, record);
                        }
                    }
                    if (listener != null) {
                        listener.onData(fetchedData);
                    }
                    Log.d(TAG, locationString);
                }
            }
        };
    }

    @SuppressLint("MissingPermission")
    private void startLocationUpdates(@NonNull final Context context) {
        Log.d(TAG, "start getting location updates");
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        LocationRequest request = createLocationRequest(NORMAL_INTERVAL, FASTEST_INTERVAL, PRIORITY);
        locationCallback = createLocationCallback();

        builder.addLocationRequest(request);
        SettingsClient settingsClient = LocationServices.getSettingsClient(context);
        settingsClient.checkLocationSettings(builder.build())
                .addOnSuccessListener((r) -> {
                    Log.d(TAG, "location settings obtained - ok");
                    locationProviderClient.requestLocationUpdates(request, locationCallback, null);
                })
                .addOnFailureListener((error) -> {
                    Log.d(TAG, "location settings obtained - error");
                    if (listener != null) {
                        listener.onFailed(error);
                    }
                })
                .addOnCanceledListener(() -> {
                    Log.d(TAG, "location settings obtained - cancelled");
                    if (listener != null) {
                        listener.onFailed(new CancellationException("location obtaining cancelled"));
                    }
                });
    }
}
