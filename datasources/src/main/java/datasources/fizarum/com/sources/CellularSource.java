package datasources.fizarum.com.sources;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.telephony.CellInfo;
import android.telephony.TelephonyManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import datasources.fizarum.com.DataSource;
import datasources.fizarum.com.OnDataFetchedListener;

import static datasources.fizarum.com.Constants.MY_PERMISSIONS_REQUEST_NETWORK_STATE;

public class CellularSource implements DataSource {

    private Context context;
    private OnDataFetchedListener listener;
    private List<Bundle> fetchedData = new ArrayList<>();

    @Override
    public List<String> getPermissions() {
        return Arrays.asList(Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.ACCESS_FINE_LOCATION);
    }

    @Override
    public int getRequestId() {
        return MY_PERMISSIONS_REQUEST_NETWORK_STATE;
    }

    @Override
    public void onPermissionGranted() {

    }

    @Override
    public void onPermissionFailed() {

    }

    @SuppressLint("MissingPermission")
    @Override
    public void onStart() {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        for(Network network : connectivityManager.getAllNetworks()) {
            NetworkInfo networkInfo = connectivityManager.getNetworkInfo(network);
            Bundle networkInfoBundle = new Bundle();
            networkInfoBundle.putString("networkInfo", networkInfo.toString());
            fetchedData.add(networkInfoBundle);
        }

        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        List<CellInfo> cellInfos = telephonyManager.getAllCellInfo();
        for(CellInfo cellInfo : cellInfos) {
            Bundle cellInfoBundle = new Bundle();
            cellInfoBundle.putString("cellInfo", cellInfo.toString());
            fetchedData.add(cellInfoBundle);
        }

        if(listener != null) {
            listener.onData(fetchedData);
        }
    }

    @Override
    public void onStop() {

    }

    @Override
    public void setContext(@NonNull Context context) {
        this.context = context;
    }

    @Override
    public List<Bundle> getData() {
        return fetchedData;
    }

    @Override
    public void setOnDataFetchListener(OnDataFetchedListener listener) {
        this.listener = listener;
    }
}
