package datasources.fizarum.com.sources;

import android.Manifest;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import datasources.fizarum.com.DataSource;
import datasources.fizarum.com.OnDataFetchedListener;

import static datasources.fizarum.com.Constants.MY_PERMISSIONS_REQUEST_READ_SMS;

public class MessagesSource implements DataSource {

    private Context context;
    private List<Bundle> fetchedData = new ArrayList<>();
    private OnDataFetchedListener listener;

    @Override
    public List<String> getPermissions() {
        return Arrays.asList(Manifest.permission.READ_SMS);
    }

    @Override
    public int getRequestId() {
        return MY_PERMISSIONS_REQUEST_READ_SMS;
    }

    @Override
    public void onPermissionGranted() {

    }

    @Override
    public void onPermissionFailed() {

    }

    @Override
    public void onStart() {
        fetchedData = readMessages(context);
        if (listener != null && fetchedData != null) {
            listener.onData(fetchedData);
        } else if (listener != null) {
            listener.onFailed(new NullPointerException());
        }
    }

    @Override
    public void onStop() {

    }

    @Override
    public void setContext(@NonNull Context context) {
        this.context = context;
    }

    @Override
    public List<Bundle> getData() {
        return fetchedData;
    }

    @Override
    public void setOnDataFetchListener(OnDataFetchedListener listener) {
        this.listener = listener;
    }

    private List<Bundle> readMessages(@NonNull final Context context) {

        List<Bundle> results = new ArrayList<>();

        Uri messageUri = Uri.parse("content://sms/");
        Cursor cursor = context.getContentResolver().query(messageUri, null, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            do {
                Bundle record = new Bundle();
                for (int idx = 0; idx < cursor.getColumnCount(); idx++) {
                    record.putString(cursor.getColumnName(idx), cursor.getString(idx));
                }
                results.add(record);
            } while (cursor.moveToNext());
            cursor.close();
        }
        return results;
    }
}
