package datasources.fizarum.com.sources;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.CallLog;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import datasources.fizarum.com.DataSource;
import datasources.fizarum.com.OnDataFetchedListener;

import static datasources.fizarum.com.Constants.MY_PERMISSIONS_REQUEST_READ_CALL_LOGS;


public class CallLogsSource implements DataSource {

    private OnDataFetchedListener listener;
    private ContentResolver contentResolver;
    private List<Bundle> fetchedData = new ArrayList<>();

    @Override
    public List<String> getPermissions() {
        return Arrays.asList(Manifest.permission.READ_CALL_LOG);
    }

    @Override
    public int getRequestId() {
        return MY_PERMISSIONS_REQUEST_READ_CALL_LOGS;
    }

    @Override
    public void onPermissionGranted() {

    }

    @Override
    public void onPermissionFailed() {

    }

    @Override
    public void onStart() {
        if (contentResolver != null) {
            fetchedData = readCallLogs(contentResolver);
            if(listener != null) {
                listener.onData(fetchedData);
            }
        } else {
            if(listener != null) {
                listener.onFailed(new NullPointerException());
            }
        }
    }

    @Override
    public void onStop() {

    }

    @Override
    public void setContext(@NonNull Context context) {
        contentResolver = context.getContentResolver();
    }

    @Override
    public List<Bundle> getData() {
        return fetchedData;
    }

    @Override
    public void setOnDataFetchListener(OnDataFetchedListener listener) {
        this.listener = listener;
    }

    private List<Bundle> readCallLogs(@NonNull ContentResolver contentResolver) {
        Cursor cursor = contentResolver.query(CallLog.Calls.CONTENT_URI, null, null, null, CallLog.Calls._ID + " DESC");

        if(cursor == null) {
            return null;
        }

        List<Bundle> result = new ArrayList<>();

        int numberIndex = cursor.getColumnIndex(CallLog.Calls.NUMBER);
        int nameIndex = cursor.getColumnIndex(CallLog.Calls.CACHED_NAME);
        int dateIndex = cursor.getColumnIndex(CallLog.Calls.DATE);
        int durationIndex = cursor.getColumnIndex(CallLog.Calls.DURATION);
        int typeIndex = cursor.getColumnIndex(CallLog.Calls.TYPE);

        while (cursor.moveToNext()) {
            String number = cursor.getString(numberIndex);
            String name = cursor.getString(nameIndex);
            String dateMillis = cursor.getString(dateIndex);
            String duration = cursor.getString(durationIndex);
            int type = cursor.getInt(typeIndex);
            String typeStr = getStringType(type);

            Bundle record = new Bundle();
            record.putString("number", number);
            record.putString("name", name);
            record.putString("date", dateMillis);
            record.putString("duration", duration);
            record.putString("type", typeStr);

            result.add(record);
        }
        cursor.close();
        return result;
    }

    private String getStringType(int callType) {
        switch (callType) {
            case CallLog.Calls.INCOMING_TYPE:
                return "income";

            case CallLog.Calls.OUTGOING_TYPE:
                return "outgoing";

            case CallLog.Calls.MISSED_TYPE:
                return "missed";

            case CallLog.Calls.REJECTED_TYPE:
                return "rejected";

            default:
                return "unknown";
        }
    }
}
