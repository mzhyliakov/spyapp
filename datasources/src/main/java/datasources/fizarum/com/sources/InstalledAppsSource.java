package datasources.fizarum.com.sources;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import datasources.fizarum.com.DataSource;
import datasources.fizarum.com.OnDataFetchedListener;

public class InstalledAppsSource implements DataSource {

    private Context context;
    private OnDataFetchedListener listener;
    private final List<Bundle> fetchedData = new ArrayList<>();

    @Override
    public List<String> getPermissions() {
        return null;
    }

    @Override
    public int getRequestId() {
        return 0;
    }

    @Override
    public void onPermissionGranted() {

    }

    @Override
    public void onPermissionFailed() {

    }

    @Override
    public void onStart() {
        final PackageManager pm = context.getPackageManager();
        //get a list of installed apps.
        List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);

        fetchedData.clear();
        for (ApplicationInfo packageInfo : packages) {
            Bundle record = new Bundle();
            record.putString("package", packageInfo.packageName);
            record.putString("directory", packageInfo.sourceDir);
            fetchedData.add(record);
        }
        if(listener != null) {
            listener.onData(fetchedData);
        }
    }

    @Override
    public void onStop() {

    }

    @Override
    public void setContext(@NonNull Context context) {
        this.context = context;
    }

    @Override
    public List<Bundle> getData() {
        return fetchedData;
    }

    @Override
    public void setOnDataFetchListener(OnDataFetchedListener listener) {
        this.listener = listener;
    }
}
